globalThis.moduleDir = __dirname;

const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));

module.exports = config => {
    Object.keys(config).forEach(configKey => {
        Config[configKey] = config[configKey];
    });
}