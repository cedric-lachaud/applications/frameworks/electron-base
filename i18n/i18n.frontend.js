$(() => {
    window.preload.ipc.getLanguages((_, language, translations) => {
        i18next
            .use(i18nextBrowserLanguageDetector)
            .init({
                debug: true,
                lng: language,
                resources: translations,
            })
            .then(() => {
                jqueryI18next.init(i18next, $, {
                    useOptionsAttr: true,
                });
                $("body").localize(window.i18nValues);
            })
            .catch(err => {
                console.error(err);
            })
        ;
    });

    window.preload.ipc.changeLanguage((_, language) => {
        i18next.changeLanguage(language);
        $("body").localize(window.i18nValues);
    });
});
