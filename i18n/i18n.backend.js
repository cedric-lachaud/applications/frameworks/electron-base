const {app} = require("electron");
const fs = require("fs");
const path = require("path");
const i18n = require("i18next");
const backend = require("i18next-fs-backend");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));

const getAvailableLanguages = () => {
    return fs.readdirSync(Config.i18nDirPath)
        .filter(language => {
            return language.length === 2;
        });
};

const loadPath = (language, namespace) => {
    let path = Config.i18nTranslationPath;

    switch (namespace) {
        case "electron-base":
            path = Config.i18nModuleTranslationPath;
            break;
        default:
            break;
    }

    return path;
}

i18n
    .use(backend)
    .init({
        backend: {
            loadPath: loadPath,
        },
        debug: Config.isDebug,
        ns: ["translation", "electron-base"],
        defaultNS: "translation",
        saveMissing: false,
        lng: app.getLocale(),
        fallbackLng: getAvailableLanguages().shift(),
        supportedLngs: getAvailableLanguages()
    });

module.exports = i18n;
