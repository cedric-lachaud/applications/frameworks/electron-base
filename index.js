const path = require("path");
const Config = require(path.join(__dirname, "classes/Config"));

Config.checkConfig();
globalThis.moduleDir = Config.moduleDir;

module.exports = {
    i18next: require("i18next"),
    i18nextBrowserLanguagedetector: require("i18next-browser-languagedetector"),
    i18nextBackend: require("i18next-electron-fs-backend"),
    i18nextFsBackend: require("i18next-fs-backend"),
    jquery: require("jquery"),
    jqueryI18next: require("jquery-i18next"),

    Config,
    i18nextMainBackend: Config.require("i18n/i18n.backend"),
    AgreeLicense: Config.require("classes/AgreeLicense"),
    MenuManager: Config.require("classes/MenuManager"),
    NotificationManager: Config.require("classes/NotificationManager"),
    WindowManager: Config.require("classes/WindowManager"),
    WindowBase: Config.require("classes/WindowBase"),
    WindowAbout: Config.require("windows/WindowAbout"),
};
