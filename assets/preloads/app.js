const {ipcRenderer, contextBridge} = require("electron");

console.log("Preload app.js");

window.id = ipcRenderer.sendSync("get-window-id");
window.iconPath = ipcRenderer.sendSync("get-icon-path").replace(/\\/g, "\\\\");

console.log(window.iconPath);

contextBridge.exposeInIsolatedWorld(window.id, "get", {
    "id": () => {
        return window.id
    },
    "iconPath": () => {
        return window.iconPath
    },
});

contextBridge.exposeInIsolatedWorld(window.id, "preload", {
    "ipc": {
        "appReady": callback => ipcRenderer.on("onAppReady", callback),
        "changeLanguage": callback => ipcRenderer.on("onChangeLanguage", callback),
        "initData": callback => ipcRenderer.on("onInitData", callback),
        "getContent": callback => ipcRenderer.on("onGetContent", callback),
        "getLanguages": callback => ipcRenderer.on("onGetLanguages", callback),
        "getVariables": callbabk => ipcRenderer.on("onGetVariables", callbabk),
        "onContentReceived": () => ipcRenderer.send("contentReceived"),
        "onI18nReceived": () => ipcRenderer.send("i18nReceived"),
        "onContentLoaded": () => ipcRenderer.send("contentLoaded"),
        "onExternalSiteRequested": url => ipcRenderer.send("externalSiteRequested", url),
        "onShowSuccess": (title, body) => ipcRenderer.send("showSuccess", title, body)
    }
});
