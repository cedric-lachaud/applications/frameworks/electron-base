$(() => {
    let content;

    $(":root").css({
        "--icon-path": `url("${window.get.iconPath()}")`
    });

    $(document).on("click", "a[href^=\"http\"]", function (event) {
        event.preventDefault();
        window.preload.ipc.onExternalSiteRequested($(this).attr("href"));
    });

    window.preload.ipc.appReady(() => {
        console.log("App is ready");
        const $item = $("#loader");
        const $container = $item.parents(".container");
        if (content) {
            $item.after(content);
        }
        $item.remove();
        $container.removeClass("d-flex align-items-center justify-content-center");
        window.preload.ipc.onContentLoaded();
    });

    window.preload.ipc.getVariables((_, i18nValues) => {
        console.log("Receive i18nValues");
        if (typeof window.i18nValues === "undefined") {
            window.i18nValues = i18nValues;
        } else {
            window.i18nValues = {...window.i18nValues, ...i18nValues};
        }
        window.preload.ipc.onI18nReceived();
    });

    window.preload.ipc.getContent((_, html) => {
        console.log("Receive content");
        content = html;
        window.preload.ipc.onContentReceived();
    });
});