const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const WindowEBModule = Config.require("classes/WindowEBModule");

class WindowAbout extends WindowEBModule {
    constructor() {
        super(
            {
                width: 940,
                height: 520,
                preloadPath: path.join(Config.moduleDir, "windows/about/preload.js"),
                layoutName: "fluid",
                viewName: "about",
                title: "About",
                resizable: false,
                movable: false,
                minimizable: false,
                maximizable: false,
                alwaysOnTop: true,
                modal: true,
                focusable: true,
                fullscreenable: false,
                skipTaskbar: true,
                frame: false,
                titleBarStyle: "hidden",
                initialData: Config.additionalCredits
            }
        );

        this.addIpcListeners({
            "closeWindow": () => {
                this.close();
            }
        });
    }
}

module.exports = WindowAbout;
