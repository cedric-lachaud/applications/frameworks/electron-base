$(() => {
    let ready = false;
    let credits = null;

    const addCredits = () => {
        if (Array.isArray(credits)) {
            credits.forEach(creditLine => {
                if (typeof creditLine === "undefined") {
                    return;
                }

                let tdName = "";
                if (typeof creditLine.name !== "undefined") {
                    tdName = `<td>${creditLine.name}</td>`;
                } else if (typeof creditLine.i18nName !== "undefined") {
                    tdName = `<td data-i18n="${creditLine.i18nName}"></td>`;
                } else {
                    return;
                }

                let tdCredits = "";
                if (typeof creditLine.text !== "undefined") {
                    if (typeof creditLine.href !== "undefined") {
                        tdCredits = `<td><a href="${creditLine.href}">${creditLine.text}</a></td>`;
                    } else {
                        tdCredits = `<td>${creditLine.text}</td>`;
                    }
                } else if (typeof creditLine.i18nText !== "undefined") {
                    if (typeof creditLine.href !== "undefined") {
                        tdCredits = `<td><a href="${creditLine.href}" data-i18n="${creditLine.i18nText}"></a></td>`;
                    } else {
                        tdCredits = `<td data-i18n="${creditLine.i18nText}"></td>`;
                    }
                } else {
                    return;
                }

                $("#credits").append(`<tr scope="row">${tdName}${tdCredits}</tr>`);
            });
        }
    }

    window.preload.ipc.initData(
        (_, additionnalCredits) => {
            credits = additionnalCredits;

            if (ready) {
                addCredits();
            }
        }
    );

    window.preload.ipc.appReady(() => {
        ready = true;

        $("#closeButton").on("click", () => {
            console.log("Close about window");
            window.about.ipc.onCloseWindow();
        });

        if (credits !== null) {
            addCredits();
        }

        console.log("About controller loaded");
    });
});
