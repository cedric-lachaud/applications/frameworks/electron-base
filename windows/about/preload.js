const {ipcRenderer, contextBridge} = require("electron");

require("../../preloads");

console.log("Preload about/preload.js", "Window", "#" + window.id);

contextBridge.exposeInIsolatedWorld(window.id, "about", {
    "ipc": {
        "onCloseWindow": () => ipcRenderer.send("closeWindow")
    }
});
