const path = require("path");
const {Config} = require("@genie23/electron-base");
const fwConfig = require("@genie23/electron-base/package.json");
const config = require(path.join(Config.projectDir, "package.json"));

module.exports = () => {
    return {
        appName: config.productName,
        fwName: fwConfig.productName,
        fwVersion: fwConfig.version,
        fwAuthorName: fwConfig.author.name,
        fwAuthorEmail: fwConfig.author.email,
    };
}