$(() => {
    let license = null;
    let appLoaded = false;

    window.preload.ipc.appReady(() => {
        $("#agreeButton").on("click", () => {
            console.log("Agree license");
            window.license.ipc.onAgreeLicense();
        });

        $("#disagreeButton").on("click", () => {
            console.log("Disagree license");
            window.license.ipc.onDisagreeLicense();
        });

        if (license !== null) {
            $("#licenseText").val(license);
        } else {
            appLoaded = true;
        }

        console.log("License controller loaded");
    });

    window.license.ipc.getLicenseText(
        (event, licenseText) => {
            license = licenseText;

            if (appLoaded) {
                $("#licenseText").val(license);
            }
        }
    );
});
