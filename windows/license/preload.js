const {ipcRenderer, contextBridge} = require("electron");

require("../../preloads");

console.log("Preload license/preload.js", "Window", "#" + window.id);

contextBridge.exposeInIsolatedWorld(window.id, "license", {
    "ipc": {
        "getLicenseText": callback => ipcRenderer.on("onGetLicenseText", callback),
        "onAgreeLicense": () => ipcRenderer.send("agreeLicense"),
        "onDisagreeLicense": () => ipcRenderer.send("disagreeLicense")
    }
});
