const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const config = require(path.join(Config.projectDir, "package.json"));

module.exports = () => {
    return {
        appName: config.productName
    };
}