const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const fs = require("fs");
const WindowEBModule = Config.require("classes/WindowEBModule");

class WindowLicense extends WindowEBModule {
    constructor(licenseText) {
        super(
            {
                width: 740,
                height: 720,
                preloadPath: path.join(Config.moduleDir, "windows/license/preload.js"),
                layoutName: "fluid",
                viewName: "license",
                title: "Agree license",
                resizable: false,
                movable: false,
                minimizable: false,
                maximizable: false,
                focusable: true,
                fullscreenable: false,
                frame: false,
                titleBarStyle: "hidden",
            }
        );

        this.addIpcListeners({
            "agreeLicense": () => {
                fs.writeFile(path.join(Config.projectDir, "eula.txt"), "agree", {}, err => {
                    if (err) {
                        console.error(err);
                    } else {
                        this.emit("agree-license");
                    }
                });
            },
            "disagreeLicense": () => {
                this.emit("disagree-license");
            }
        });

        this.addWindowListeners({
            "ready": () => {
                this.webContents.send("onGetLicenseText", licenseText);
            }
        });
    }
}

module.exports = WindowLicense;