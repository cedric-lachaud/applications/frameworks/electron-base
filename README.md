# Framework Electron Base for ElectronJS

![Logo Genie23](assets/imgs/logos/genie23.text.svg)

Electron Base is a framework developed from and for ElectronJS, to make it as easy as possible to create desktop
applications with ElectronJS.

The framework has been developed to automate repetitive tasks such as window management, menu management and application
internationalization.

> :warning: Project under development ! Do not use for the moment !