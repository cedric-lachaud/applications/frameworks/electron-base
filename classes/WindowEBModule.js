const path = require("path");
const fs = require("fs");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const WindowBase = Config.require("classes/WindowBase");

class WindowEBModule extends WindowBase {
    get cssPath() {
        return path.join(Config.moduleDir, "windows", this.viewName, "style.css");
    }

    get controllerPath() {
        return path.join(Config.moduleDir, "windows", this.viewName, "controller.js");
    }

    get i18nTranslationPath() {
        return path.join(Config.moduleDir, "windows", this.viewName, "values.js");
    }

    get i18nValuesPath() {
        return path.join(Config.moduleDir, "windows", this.viewName, "values.js");
    }

    get contentPath() {
        return path.join(Config.moduleDir, "windows", this.viewName, "content.html");
    }
}

module.exports = WindowEBModule;