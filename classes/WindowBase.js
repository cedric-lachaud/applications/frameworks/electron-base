const {BrowserWindow, ipcMain, screen, session} = require("electron");
const fs = require("fs");
const path = require("path");

const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const i18nextMainBackend = Config.require("i18n/i18n.backend");
const WindowManager = Config.require("classes/WindowManager");

class WindowBase extends BrowserWindow {
    #addedCss;
    #executedJs;
    #properties;
    #windowId;
    #windowName;
    #layoutLoaded;
    #contentLoaded;

    #i18nLoaded;
    #scriptsLoaded;
    #stylesLoaded;

    #jsToExecute;
    #cssToUse;
    #contentToLoad;
    #i18nValuesToLoad;

    #launchUseCss;
    #launchExecuteJs;

    constructor(properties) {
        const getWindowId = event => {
            ipcMain.removeListener("get-window-id", getWindowId);
            event.returnValue = this.id;
        }

        ipcMain.on("get-window-id", getWindowId);

        if (typeof properties.layoutName === "undefined" || properties.layoutName === null) {
            throw new Error("Layout name must be defined");
        }

        if (typeof properties.viewName === "undefined" || properties.viewName === null) {
            throw new Error("View name must be defined");
        }

        // const preloadPath = properties.viewName;
        let preloadPath = null;
        if (typeof properties.preloadPath !== "undefined") {
            if (fs.existsSync(properties.preloadPath)) {
                preloadPath = properties.preloadPath;
            }
        }
        if (preloadPath === null) {
            if (fs.existsSync(Config.getPreloadPath(properties.viewName))) {
                preloadPath = Config.getPreloadPath(properties.viewName);
            } else {
                preloadPath = path.join(Config.moduleDir, "preloads.js");
            }
        }

        const {screenWidth, screenHeight} = screen.getPrimaryDisplay().workAreaSize;

        super({
            width: typeof properties.width !== "undefined" ? properties.width : 1280,
            height: typeof properties.height !== "undefined" ? properties.height : 720,
            minWidth: typeof properties.minWidth !== "undefined" ? properties.minWidth : 1020,
            minHeight: typeof properties.minHeight !== "undefined" ? properties.minHeight : 640,
            maxWidth: typeof properties.maxWidth !== "undefined" ? properties.maxWidth : screenWidth,
            maxHeight: typeof properties.maxHeight !== "undefined" ? properties.maxHeight : screenHeight,
            icon: typeof properties.icon !== 'undefined' ? properties.icon : Config.getIconPath(properties.viewName),
            backgroundColor: typeof properties.backgroundColor !== "undefined" ? properties.backgroundColor : "white",
            parent: typeof properties.parent !== "undefined" ? properties.parent : Config.mainWindow,
            modal: typeof properties.modal !== "undefined" ? properties.modal : false,
            resizable: typeof properties.resizable !== "undefined" ? properties.resizable : true,
            movable: typeof properties.movable !== "undefined" ? properties.movable : true,
            minimizable: typeof properties.minimizable !== "undefined" ? properties.minimizable : true,
            maximizable: typeof properties.maximizable !== "undefined" ? properties.maximizable : true,
            closable: typeof properties.closable !== "undefined" ? properties.closable : true,
            focusable: typeof properties.focusable !== "undefined" ? properties.focusable : true,
            alwaysOnTop: typeof properties.alwaysOnTop !== "undefined" ? properties.alwaysOnTop : false,
            fullscreen: typeof properties.fullscreen !== "undefined" ? properties.fullscreen : false,
            fullscreenable: typeof properties.fullscreenable !== "undefined" ? properties.fullscreenable : true,
            simpleFullscreen: typeof properties.simpleFullscreen !== "undefined" ? properties.simpleFullscreen : false,
            skipTaskbar: typeof properties.skipTaskbar !== "undefined" ? properties.skipTaskbar : false,
            hiddenInMissionControl: typeof properties.hiddenInMissionControl !== "undefined" ? properties.hiddenInMissionControl : false,
            kiosk: typeof properties.kiosk !== "undefined" ? properties.kiosk : false,
            title: typeof properties.title !== "undefined" ? properties.title : "Electron",
            paintWhenInitiallyHidden: typeof properties.paintWhenInitiallyHidden !== "undefined" ? properties.paintWhenInitiallyHidden : true,
            frame: typeof properties.frame !== "undefined" ? properties.frame : true,
            acceptFirstMouse: typeof properties.acceptFirstMouse !== "undefined" ? properties.acceptFirstMouse : false,
            disableAutoHideCursor: typeof properties.disableAutoHideCursor !== "undefined" ? properties.disableAutoHideCursor : false,
            autoHideMenuBar: typeof properties.autoHideMenuBar !== "undefined" ? properties.autoHideMenuBar : false,
            enableLargerThanScreen: typeof properties.enableLargerThanScreen !== "undefined" ? properties.enableLargerThanScreen : false,
            hasShadow: typeof properties.hasShadow !== "undefined" ? properties.hasShadow : true,
            opacity: typeof properties.opacity !== "undefined" ? properties.opacity : 1.0,
            darkTheme: typeof properties.darkTheme !== "undefined" ? properties.darkTheme : false,
            transparent: typeof properties.transparent !== "undefined" ? properties.transparent : false,
            type: typeof properties.type !== "undefined" ? properties.type : null,
            titleBarStyle: typeof properties.titleBarStyle !== "undefined" ? properties.titleBarStyle : "default",
            roundedCorners: typeof properties.roundedCorners !== "undefined" ? properties.roundedCorners : true,
            thickFrame: typeof properties.thickFrame !== "undefined" ? properties.thickFrame : true,
            zoomToPageWidth: typeof properties.zoomToPageWidth !== "undefined" ? properties.zoomToPageWidth : false,
            titleBarOverlay: typeof properties.titleBarOverlay !== "undefined" ? properties.titleBarOverlay : false,
            show: false,
            webPreferences: {
                devTools: typeof properties.devTools !== "undefined" ? properties.devTools : Config.isDebug,
                nodeIntegration: typeof properties.nodeIntegration !== "undefined" ? properties.nodeIntegration : false,
                nodeIntegrationInWorker: typeof properties.nodeIntegrationInWorker !== "undefined" ? properties.nodeIntegrationInWorker : false,
                contextIsolation: typeof properties.contextIsolation !== "undefined" ? properties.contextIsolation : true,
                enableRemoteModule: typeof properties.enableRemoteModule !== "undefined" ? properties.enableRemoteModule : false,
                preload: preloadPath,
                sandbox: false,
                session: typeof properties.session !== "undefined" ? properties.session : session.defaultSession,
                partition: typeof properties.partition !== "undefined" ? properties.partition : null,
                zoomFactor: typeof properties.zoomFactor !== "undefined" ? properties.zoomFactor : 1.0,
                javascript: typeof properties.javascript !== "undefined" ? properties.javascript : true,
                webSecurity: typeof properties.webSecurity !== "undefined" ? properties.webSecurity : true,
                allowRunningInsecureContent: typeof properties.allowRunningInsecureContent !== "undefined" ? properties.allowRunningInsecureContent : false,
                images: typeof properties.images !== "undefined" ? properties.images : true,
                imageAnimationPolicy: typeof properties.imageAnimationPolicy !== "undefined" ? properties.imageAnimationPolicy : "animate",
                textAreasAreResizable: typeof properties.textAreasAreResizable !== "undefined" ? properties.textAreasAreResizable : true,
                webgl: typeof properties.webgl !== "undefined" ? properties.webgl : true,
                plugins: typeof properties.plugins !== "undefined" ? properties.plugins : false,
                experimentalFeatures: typeof properties.experimentalFeatures !== "undefined" ? properties.experimentalFeatures : false,
                scrollBounce: typeof properties.scrollBounce !== "undefined" ? properties.scrollBounce : false,
                enableBlinkFeatures: typeof properties.enableBlinkFeatures !== "undefined" ? properties.enableBlinkFeatures : null,
                disableBlinkFeatures: typeof properties.disableBlinkFeatures !== "undefined" ? properties.disableBlinkFeatures : null,
                defaultFontFamily: typeof properties.defaultFontFamily !== "undefined" ? properties.defaultFontFamily : "standard",
                defaultFontSize: typeof properties.defaultFontSize !== "undefined" ? properties.defaultFontSize : 16,
                defaultMonospaceFontSize: typeof properties.defaultMonospaceFontSize !== "undefined" ? properties.defaultMonospaceFontSize : 13,
                minimumFontSize: typeof properties.minimumFontSize !== "undefined" ? properties.minimumFontSize : 0,
                defaultEncoding: typeof properties.defaultEncoding !== "undefined" ? properties.defaultEncoding : "UTF-8",
                backgroundThrottling: typeof properties.backgroundThrottling !== "undefined" ? properties.backgroundThrottling : true,
                offscreen: typeof properties.offscreen !== "undefined" ? properties.offscreen : false,
                webviewTag: typeof properties.webviewTag !== "undefined" ? properties.webviewTag : false,
                additionalArguments: typeof properties.additionalArguments !== "undefined" ? properties.additionalArguments : [],
                safeDialogs: typeof properties.safeDialogs !== "undefined" ? properties.safeDialogs : false,
                safeDialogsMessage: typeof properties.safeDialogsMessage !== "undefined" ? properties.safeDialogsMessage : null,
                disableDialogs: typeof properties.disableDialogs !== "undefined" ? properties.disableDialogs : false,
                navigateOnDragDrop: typeof properties.navigateOnDragDrop !== "undefined" ? properties.navigateOnDragDrop : false,
                autoplayPolicy: typeof properties.autoplayPolicy !== "undefined" ? properties.autoplayPolicy : "no-user-gesture-required",
                disableHtmlFullscreenWindowResize: typeof properties.disableHtmlFullscreenWindowResize !== "undefined" ? properties.disableHtmlFullscreenWindowResize : false,
                accessibleTitle: typeof properties.accessibleTitle !== "undefined" ? properties.accessibleTitle : null,
                spellcheck: typeof properties.spellcheck !== "undefined" ? properties.spellcheck : true,
                enableWebSQL: typeof properties.enableWebSQL !== "undefined" ? properties.enableWebSQL : true,
                v8CacheOptions: typeof properties.v8CacheOptions !== "undefined" ? properties.v8CacheOptions : "code",
                enablePreferredSizeMode: typeof properties.enablePreferredSizeMode !== "undefined" ? properties.enablePreferredSizeMode : false
            }
        });

        const listeners = {
            window: properties.windowListeners,
            webContents: properties.webContentsListeners,
            ipc: properties.ipcListeners
        };

        this.#launchUseCss = false;
        this.#cssToUse = [];
        this.#launchExecuteJs = false;
        this.#jsToExecute = [];

        this.#windowName = this.constructor.name;
        this.#windowId = typeof properties.allowMultipleInstances !== "undefined" && properties.allowMultipleInstances ? crypto.randomUUID() : `${this.#windowName}_unique`;
        this.#properties = properties;
        console.log("Creating", this.#windowName);

        this.#properties.windowListeners = {};

        this.#properties.webContentsListeners = {};
        this.#properties.ipcListeners = {};
        this.addWindowListeners(typeof listeners.window !== "undefined" ? listeners.window : {});

        this.addWebContentsListeners(typeof listeners.webContents !== "undefined" ? listeners.webContents : {});
        this.addIpcListeners(typeof listeners.ipc !== "undefined" ? listeners.ipc : {});
        this.#properties.initialData = this.#properties.initialData || null;

        this.addIpcListeners({
            "contentReceived": () => {
                this.setContentIsLoaded();
            },
            "i18nReceived": () => {
                this.setI18nIsLoaded();
            },
        });

        this.addWindowListeners({
            "failedToLoad": () => {
                return new this.constructor(this.#properties);
            },
            "close": () => {
                Object.keys(this.webContentsListeners).forEach(event => {
                    if (typeof this.webContentsListeners[event] !== "undefined") {
                        const func = this.webContentsListeners[event];
                        if (Array.isArray(func)) {
                            func.forEach(f => {
                                this.webContents.removeListener(event, f);
                            });
                        } else if (typeof func === "function") {
                            this.webContents.removeListener(event, func);
                        }
                        delete this.webContentsListeners[event];
                    }
                });
                Object.keys(this.ipcListeners).forEach(event => {
                    if (typeof this.ipcListeners[event] !== "undefined") {
                        const func = this.ipcListeners[event];
                        if (Array.isArray(func)) {
                            func.forEach(f => {
                                ipcMain.removeListener(event, f);
                            });
                        } else {
                            ipcMain.removeListener(event, func);
                        }
                        delete this.ipcListeners[event];
                    }
                });
            },
            "closed": () => {
                Object.keys(this.windowListeners).sort((a, b) => {
                    if (a !== "closed" && b === "closed") {
                        return -1;
                    } else if (a === "closed" && b !== "closed") {
                        return 1;
                    } else {
                        return 0;
                    }
                }).forEach(event => {
                    if (typeof this.windowListeners[event] !== "undefined") {
                        const func = this.windowListeners[event];
                        if (Array.isArray(func)) {
                            func.forEach(f => {
                                this.removeListener(event, f);
                            });
                        } else {
                            this.removeListener(event, func);
                            delete this.windowListeners[event];
                        }
                    }
                });
            }
        });

        if (properties.show) {
            this.show();
        }
    }

    get windowId() {
        return this.#windowId;
    }

    get windowName() {
        return this.#windowName;
    }

    get menuName() {
        return this.#properties.menuName;
    }

    get layoutName() {
        return this.#properties.layoutName;
    }

    get viewName() {
        return this.#properties.viewName;
    }

    get cssPath() {
        return Config.getStylePath(this.viewName);
    }

    get controllerPath() {
        return Config.getControllerPath(this.viewName);
    }

    get i18nValuesPath() {
        return Config.getI18nValuesPath(this.viewName);
    }

    get contentPath() {
        return Config.getViewPath(this.viewName);
    }

    get initialData() {
        return this.#properties.initialData;
    }

    get windowListeners() {
        return this.#properties.windowListeners;
    }

    get webContentsListeners() {
        return this.#properties.webContentsListeners;
    }

    get ipcListeners() {
        return this.#properties.ipcListeners;
    }

    get isLayoutLoaded() {
        return this.#layoutLoaded;
    }

    get isContentLoaded() {
        return this.#contentLoaded;
    }

    get isI18nLoaded() {
        return this.#i18nLoaded;
    }

    get isScriptsLoaded() {
        return this.#scriptsLoaded;
    }

    get isStylesLoaded() {
        return this.#stylesLoaded;
    }

    get shown() {
        return this.isVisible();
    }

    isAppLoaded() {
        return this.isLayoutLoaded && this.isScriptsLoaded && this.isStylesLoaded;
    }

    isAppReady() {
        return this.isContentLoaded && this.isI18nLoaded && this.isScriptsLoaded && this.isStylesLoaded;
    }

    #checkLoaded() {
        if (this.isAppLoaded()) {
            console.log("App loaded");

            this.emit("loaded");
        }
    }

    #checkReady() {
        if (this.isAppReady()) {
            console.log("App ready");

            this.emit("ready");
        }
    }

    setLayoutIsLoaded() {
        this.#layoutLoaded = true;

        console.log("Layout loaded");

        this.#checkLoaded();
    }

    setContentIsLoaded() {
        this.#contentLoaded = true;

        console.log("Content loaded");

        this.#checkReady();
    }

    setI18nIsLoaded() {
        this.#i18nLoaded = true;

        console.log("i18n loaded");

        this.#checkReady();
    }

    setScriptsAreLoaded() {
        this.#launchExecuteJs = false;
        this.#scriptsLoaded = true;

        console.log("Scripts loaded");


        this.#checkLoaded();
        this.#checkReady();
    }

    setStylesAreLoaded() {
        this.#launchUseCss = false;
        this.#stylesLoaded = true;

        console.log("Styles loaded");

        this.#checkLoaded();
        this.#checkReady();
    }

    prepareWindow() {
        this.#addedCss = [];
        this.#executedJs = [];

        this.#layoutLoaded = false;
        this.#contentLoaded = false;
        this.#i18nLoaded = false;
        this.#scriptsLoaded = false;
        this.#stylesLoaded = false;

        this.initializeWindow();
    }

    initializeWindow() {
        const getWindowId = event => {
            ipcMain.removeListener("get-window-id", getWindowId);
            event.returnValue = this.id;
        }
        ipcMain.on("get-window-id", getWindowId);

        const getIconPath = event => {
            ipcMain.removeListener("get-icon-path", getIconPath);
            event.returnValue = Config.getIconPath();
        }
        ipcMain.on("get-icon-path", getIconPath);

        const onAppLoaded = () => {
            this.removeListener("loaded", onAppLoaded);

            this.#layoutLoaded = false;
            this.#scriptsLoaded = false;
            this.#stylesLoaded = false;

            this.prepareLoadContent();

            this.addIpcListeners({
                "externalSiteRequested": WindowManager.externalSiteRequested,
                "contentLoaded": () => {
                    this.webContents.once("did-start-loading", () => {
                        this.resetWindow();
                    });
                }
            });
        };

        const onAppReadyToStartLoadingContent = () => {
            this.removeListener("ready-to-start-loading-content", onAppReadyToStartLoadingContent);
            this.startLoadContent();
        };

        const onAppReady = () => {
            this.removeListener("ready", onAppReady);
            this.webContents.send("onAppReady");
        };

        this.addWindowListeners({
            "loaded": onAppLoaded,
            "ready-to-start-loading-content": onAppReadyToStartLoadingContent,
            "ready": onAppReady
        });

        this.addWebContentsListeners({
            "did-start-loading": () => {
                this.setStyles(WindowManager.globalCssPaths).then(() => {
                    this.useStyles();
                });
            },
            "dom-ready": () => {
                this.setScripts(WindowManager.globalJsPaths).then(() => {
                    this.executeScripts();
                });
            },
        });

        const layoutPath = Config.getLayoutPath(this.layoutName);
        this.loadFile(layoutPath).then(() => {
            console.log("Load layout :", layoutPath, "on window", this.id);
            this.setLayoutIsLoaded();
        }).catch(err => {
            console.error(err);
        });
    }

    addWindowListeners(newListeners, addBefore = false) {
        for (const [event, func] of Object.entries(newListeners)) {
            if (Array.isArray(func)) {
                func.forEach(f => {
                    this.addWindowListeners(event, f);
                });
            } else if (typeof func === 'object' && func !== null) {
                if (func.once) {
                    this.once(event, func.f);
                } else {
                    this.addWindowListeners(event, func.f);
                }
            } else {
                if (typeof this.#properties.windowListeners[event] === "undefined") {
                    this.#properties.windowListeners[event] = func;
                } else {
                    if (addBefore) {
                        if (Array.isArray(this.#properties.windowListeners[event])) {
                            this.#properties.windowListeners[event].unshift(func);
                        } else {
                            this.#properties.windowListeners[event] = [func, this.#properties.windowListeners[event]];
                        }
                    } else {
                        if (Array.isArray(this.#properties.windowListeners[event])) {
                            this.#properties.windowListeners[event].push(func);
                        } else {
                            this.#properties.windowListeners[event] = [this.#properties.windowListeners[event], func];
                        }
                    }
                }
                this.on(event, func);
            }
        }
    }

    addWebContentsListeners(newListeners, addBefore = false) {
        for (const [event, func] of Object.entries(newListeners)) {
            if (Array.isArray(func)) {
                func.forEach(f => {
                    this.addWebContentsListeners(event, f);
                });
            } else if (typeof func === 'object' && func !== null) {
                if (func.once) {
                    this.webContents.once(event, func.f);
                } else {
                    this.addWebContentsListeners(event, func.f);
                }
            } else {
                if (typeof this.#properties.webContentsListeners[event] === "undefined") {
                    this.#properties.webContentsListeners[event] = func;
                } else {
                    if (addBefore) {
                        if (Array.isArray(this.#properties.webContentsListeners[event])) {
                            this.#properties.webContentsListeners[event].unshift(func);
                        } else {
                            this.#properties.webContentsListeners[event] = [func, this.#properties.webContentsListeners[event]];
                        }
                    } else {
                        if (Array.isArray(this.#properties.webContentsListeners[event])) {
                            this.#properties.webContentsListeners[event].push(func);
                        } else {
                            this.#properties.webContentsListeners[event] = [this.#properties.webContentsListeners[event], func];
                        }
                    }
                }
                this.webContents.on(event, func);
            }
        }
    }

    addIpcListeners(newListeners, addBefore = false) {
        for (const [event, func] of Object.entries(newListeners)) {
            if (Array.isArray(func)) {
                func.forEach(f => {
                    this.addIpcListeners(event, f);
                });
            } else if (typeof func === 'object' && func !== null) {
                if (func.once) {
                    ipcMain.once(event, func.f);
                } else {
                    this.addIpcListeners(event, func.f);
                }
            } else {
                if (typeof this.#properties.ipcListeners[event] === "undefined") {
                    this.#properties.ipcListeners[event] = func;
                } else {
                    if (addBefore) {
                        if (Array.isArray(this.#properties.ipcListeners[event])) {
                            this.#properties.ipcListeners[event].unshift(func);
                        } else {
                            this.#properties.ipcListeners[event] = [func, this.#properties.ipcListeners[event]];
                        }
                    } else {
                        if (Array.isArray(this.#properties.ipcListeners[event])) {
                            this.#properties.ipcListeners[event].push(func);
                        } else {
                            this.#properties.ipcListeners[event] = [this.#properties.ipcListeners[event], func];
                        }
                    }
                }
                ipcMain.on(event, func);
            }
        }
    }

    prepareLoadContent() {
        Promise.all(
            [
                this.setStyles([this.cssPath]),
                this.setScripts([this.controllerPath]),
                this.setI18nValues(this.i18nValuesPath),
                this.setContent(this.contentPath)
            ]
        ).then(() => {
            this.emit("ready-to-start-loading-content");
        });
    }

    startLoadLayout() {
        this.useStyles();
        this.executeScripts();
    }

    startLoadContent() {
        const sendInitData = () => {
            if (this.initialData !== null) {
                if (!this.isDestroyed()) {
                    this.webContents.send("onInitData", this.initialData);
                } else {
                    this.emit("failedToLoad");
                }
            }
        }

        const sendTranslation = () => {
            if (WindowManager.translations === null) {
                setTimeout(sendTranslation, 100);
            } else {
                if (!this.isDestroyed()) {
                    this.webContents.send("onGetLanguages", WindowManager.currentLanguage, WindowManager.translations);
                } else {
                    this.emit("failedToLoad");
                }
            }
        };

        this.addIpcListeners({
            "contentLoaded": {
                f: () => {
                    sendInitData();
                    sendTranslation();
                },
                once: true
            }
        });

        this.useStyles();
        this.loadContent();
        this.executeScripts();
        this.loadI18nValues();
    }

    setStyles(cssPaths) {
        return new Promise(resolve => {
            this.#cssToUse = this.#cssToUse.concat(cssPaths);
            this.#launchUseCss = true;
            resolve();
        });
    }

    useStyles() {
        if (this.#launchUseCss) {
            this.#useCss();
        } else if (this.#cssToUse.length === 0) {
            this.setStylesAreLoaded();
        }
    }

    #useCss() {
        const cssPath = this.#cssToUse.shift();

        if (typeof cssPath === "undefined") {
            this.setStylesAreLoaded();
            return;
        }

        if (this.#addedCss.includes(cssPath)) {
            this.#useCss();
        }

        console.log("Loading CSS :", cssPath, "on window", this.id);

        fs.lstat(cssPath, (err, stats) => {
            if (err) {
                console.error(err);
                this.#useCss();
            } else if (stats.isFile()) {
                fs.readFile(cssPath, {}, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        if (!this.isDestroyed()) {
                            this.webContents.insertCSS(data.toString()).then(() => {
                                console.log("Used CSS :", cssPath, "on window", this.id);
                                this.#addedCss.push(cssPath);
                                this.#useCss();
                            }).catch(err => {
                                console.error(err);
                                this.#useCss();
                            });
                        } else {
                            this.emit("failedToLoad");
                        }
                    }
                });
            }
        });
    }

    setContent(contentPath) {
        return new Promise(resolve => {
            fs.lstat(contentPath, (err, stats) => {
                if (!err && stats.isFile()) {
                    this.#contentToLoad = contentPath;
                    resolve();
                }
            });
        });
    }

    loadContent() {
        if (!this.isStylesLoaded) {
            setTimeout(() => {
                this.loadContent();
            }, 50);
            return;
        }

        if (this.#contentToLoad === null) {
            throw new Error(`No content to load for ${this.windowName} window`);
        }

        const data = fs.readFileSync(this.#contentToLoad);
        if (!this.isDestroyed()) {
            this.webContents.send("onGetContent", data.toString());
            console.log("Load content :", this.#contentToLoad, "on window", this.id);
        } else {
            this.emit("failedToLoad");
        }
    }

    setScripts(jsPaths) {
        return new Promise(resolve => {
            this.#jsToExecute = this.#jsToExecute.concat(jsPaths);
            this.#launchExecuteJs = true;
            resolve();
        });
    }

    executeScripts() {
        if (this.#launchExecuteJs) {
            this.#executeJs();
        } else if (this.#jsToExecute.length === 0) {
            this.setScriptsAreLoaded();
        }
    }

    #executeJs() {
        if (!this.isStylesLoaded || (!this.isLayoutLoaded && !this.isContentLoaded)) {
            setTimeout(() => {
                this.#executeJs();
            }, 50);
            return;
        }

        const jsPath = this.#jsToExecute.shift();

        if (typeof jsPath === "undefined") {
            this.setScriptsAreLoaded();
            return;
        }

        if (this.#executedJs.includes(jsPath)) {
            this.#executeJs();
        }

        console.log("Loading JS :", jsPath, "on window", this.id);

        fs.lstat(jsPath, (err, stats) => {
            if (err) {
                console.error(err);
                this.#executeJs();
            } else if (stats.isFile()) {
                fs.readFile(jsPath, {}, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        if (!this.isDestroyed()) {
                            this.webContents.executeJavaScriptInIsolatedWorld(this.id, [{code: data.toString() + ";0"}]).then(() => {
                                console.log("Executed JS :", jsPath, "on window", this.id);
                                this.#executedJs.push(jsPath);
                                this.#executeJs();
                            }).catch(err => {
                                console.error(err);
                                this.#executeJs();
                            });
                        } else {
                            this.emit("failedToLoad");
                        }
                    }
                });
            }
        });
    }

    setI18nValues(i18nValuesPath) {
        return new Promise(resolve => {
            fs.lstat(i18nValuesPath, (err, stats) => {
                if (!err && stats.isFile()) {
                    this.#i18nValuesToLoad = i18nValuesPath;
                    resolve();
                }
            });
        });
    }

    loadI18nValues() {
        if (!this.isStylesLoaded || (!this.isLayoutLoaded && !this.isContentLoaded) || !this.isScriptsLoaded) {
            setTimeout(() => {
                this.loadI18nValues();
            }, 50);
            return;
        }

        if (this.#i18nValuesToLoad === null) {
            this.setI18nIsLoaded();
            return;
        }

        const i18nValues = require(this.#i18nValuesToLoad)(i18nextMainBackend);
        if (!this.isDestroyed()) {
            this.webContents.send("onGetVariables", i18nValues);
            console.log("Set i18nValues :", this.#i18nValuesToLoad, "on window", this.id);
        } else {
            this.emit("failedToLoad");
        }
    }
}

module.exports = WindowBase;