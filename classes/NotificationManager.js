const notifier = require("node-notifier");
const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const config = require(Config.projectPath("package.json"));

class NotificationManager {
    static #show(title, message, icon, callback = () => {
    }) {
        notifier.notify(
            {
                appID: typeof config.productName !== "undefined" ? config.productName : config.name,
                title,
                message,
                icon,
                sound: true,
                wait: true
            },
            function (err, response, metadata) {
                if (metadata.action === "clicked") {
                    callback();
                }
            }
        );
    }

    static showSuccess(title, message, callback = () => {
    }) {
        this.#show(title, message, path.join(globalThis.moduleDir, "assets/imgs/notifications/success.png"), callback);
    }

    static showWarning(title, message, callback = () => {
    }) {
        this.#show(title, message, path.join(globalThis.moduleDir, "assets/imgs/notifications/warning.png"), callback);
    }

    static showError(title, message, callback = () => {
    }) {
        this.#show(title, message, path.join(globalThis.moduleDir, "assets/imgs/notifications/error.png"), callback);
    }
}

module.exports = NotificationManager;
