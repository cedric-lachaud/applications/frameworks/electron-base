const {app} = require("electron");
const path = require("path");

class Config {
    static #mainWindow = null;
    static #layoutPathSchema = null;
    static #viewPathSchema = null;
    static #stylePathSchema = null;
    static #preloadPathSchema = null;
    static #controllerPathSchema = null;
    static #i18nDirPath = null;
    static #i18nModuleDirPath = this.modulePath("assets/i18n", true);
    static #i18nTranslationPathSchema = null;
    static #i18nModuleTranslationPathSchema = this.modulePath("assets/i18n/{{lng}}/{{ns}}.json", true);
    static #i18nValuesPathSchema = null;
    static #iconPathSchema = null;
    static #menuPathSchema = null;
    static #contextMenuPathSchema = null;
    static #licenseWindowClassPath = this.modulePath("windows/WindowLicense");
    static #additionalCredits = {};
    static #requiredModules = {};

    static get mainWindow() {
        return this.#mainWindow;
    }

    static set mainWindow(window) {
        if (window !== null) {
            let proto = window.constructor;
            while (["WindowBase", "Object"].includes(proto.name)) {
                proto = Object.getPrototypeOf(proto);
            }
            if (proto.name === "Object") {
                return;
            }
        }
        this.#mainWindow = window;
    }

    static set layoutPath(pathSchema) {
        this.#layoutPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set viewPath(pathSchema) {
        this.#viewPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set stylePath(pathSchema) {
        this.#stylePathSchema = path.join(this.projectDir, pathSchema);
    }

    static set preloadPath(pathSchema) {
        this.#preloadPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set controllerPath(pathSchema) {
        this.#controllerPathSchema = path.join(this.projectDir, pathSchema);
    }

    static get i18nDirPath() {
        return this.#i18nDirPath;
    }

    static set i18nDirPath(pathSchema) {
        this.#i18nDirPath = path.join(this.projectDir, pathSchema);
    }

    static get i18nModuleDirPath() {
        return this.#i18nModuleDirPath;
    }

    static get i18nModuleTranslationPath() {
        return this.#i18nModuleTranslationPathSchema;
    }

    static get i18nTranslationPath() {
        return this.#i18nTranslationPathSchema;
    }

    static set i18nTranslationPath(pathSchema) {
        this.#i18nTranslationPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set i18nValuesPath(pathSchema) {
        this.#i18nValuesPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set iconPath(pathSchema) {
        this.#iconPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set menuPath(pathSchema) {
        this.#menuPathSchema = path.join(this.projectDir, pathSchema);
    }

    static set contextMenuPath(pathSchema) {
        this.#contextMenuPathSchema = path.join(this.projectDir, pathSchema);
    }

    static get licenseWindowClassPath() {
        return this.#licenseWindowClassPath;
    }

    static set licenseWindowClassPath(licenseWindowClassPath) {
        this.#licenseWindowClassPath = path.join(this.projectDir, licenseWindowClassPath);
    }

    static get additionalCredits() {
        return this.#additionalCredits;
    }

    static set additionalCredits(newCredits) {
        this.#additionalCredits = newCredits;
    }

    static get isDebug() {
        return !app.isPackaged;
    }

    static get moduleDir() {
        return path.join(__dirname, "..");
    }

    static get projectDir() {
        return path.join(this.moduleDir, "../../..");
    }

    static checkConfig() {
        let errors = [];

        if (this.#layoutPathSchema === null) {
            errors.push("Layouts path schema (config key : layoutPath) not defined");
        }
        if (this.#viewPathSchema === null) {
            errors.push("Views path schema (config key : viewPath) not defined");
        }
        if (this.#stylePathSchema === null) {
            errors.push("Styles path schema (config key : stylePath) not defined");
        }
        if (this.#preloadPathSchema === null) {
            errors.push("Preloads path schema (config key : preloadPath) not defined");
        }
        if (this.#controllerPathSchema === null) {
            errors.push("Controllers path schema (config key : controllerPath) not defined");
        }
        if (this.#i18nDirPath === null) {
            errors.push("i18n directory path (config key : i18nDirPath) not defined");
        }
        if (this.#i18nTranslationPathSchema === null) {
            errors.push("i18n translation path schema (config key : i18nTranslationPath) not defined");
        }
        if (this.#i18nValuesPathSchema === null) {
            errors.push("i18n values path schema (config key : i18nValuesPath) not defined");
        }
        if (this.#iconPathSchema === null) {
            errors.push("Icon path schema (config key : iconPath) not defined");
        }
        if (this.#menuPathSchema === null) {
            errors.push("Menu path schema (config key : menuPath) not defined");
        }
        if (this.#contextMenuPathSchema === null) {
            errors.push("Context menu path schema (config key : contextMenuPath) not defined");
        }

        if (errors.length > 0) {
            throw new Error(errors.join("\n"));
        }
    }

    static getLayoutPath(layout = "") {
        return this.#layoutPathSchema.replaceAll("{{layout}}", layout);
    }

    static getViewPath(name = "") {
        return this.#viewPathSchema.replaceAll("{{name}}", name);
    }

    static getStylePath(name = "") {
        return this.#stylePathSchema.replaceAll("{{name}}", name);
    }

    static getPreloadPath(name = "") {
        return this.#preloadPathSchema.replaceAll("{{name}}", name);
    }

    static getControllerPath(name = "") {
        return this.#controllerPathSchema.replaceAll("{{name}}", name);
    }

    static getI18nValuesPath(name = "") {
        return this.#i18nValuesPathSchema.replaceAll("{{name}}", name);
    }

    static getIconPath(name = "") {
        return this.#iconPathSchema.replaceAll("{{name}}", name);
    }

    static getMenuPath(menu) {
        return this.#menuPathSchema.replaceAll("{{menu}}", menu);
    }

    static getContextMenuPath(menu) {
        return this.#contextMenuPathSchema.replaceAll("{{menu}}", menu);
    }

    static modulePath(p, relativeToProject = false) {
        if (relativeToProject) {
            return path.relative(this.projectDir, path.join(this.moduleDir, p));
        } else {
            return path.join(this.moduleDir, p);
        }
    }

    static projectPath(p) {
        return path.join(this.projectDir, p);
    }

    static require(classPath) {
        if (typeof this.#requiredModules[classPath] === "undefined") {
            this.#requiredModules[classPath] = require(path.join(this.moduleDir, classPath));
        }
        return this.#requiredModules[classPath];
    }
}

module.exports = Config;