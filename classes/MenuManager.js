const {Menu} = require("electron");
const fs = require("fs");
const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));

class MenuManager {
    initMenu(i18nextMainBackend, window) {
        if(typeof window.menuName === "undefined") {
            return;
        }

        const menuPath = Config.getMenuPath(window.menuName);
        fs.lstat(menuPath, (err, stats) => {
            if(!err) {
                const template = require(menuPath)(i18nextMainBackend);
                const menu = Menu.buildFromTemplate(template);

                if (process.platform === "darwin") {
                    Menu.setApplicationMenu(menu);
                } else {
                    window.setMenu(menu);
                }
            }
        });
    }

    initContextMenu(i18nextMainBackend, menuId) {
        const menuPath = Config.getContextMenuPath(menuId);
        fs.lstat(menuPath, (err, stats) => {
            if(!err) {
                const template = require(menuPath)(i18nextMainBackend);
                const menu = Menu.buildFromTemplate(template);
                menu.popup();
            }
        });
    }
}

module.exports = new MenuManager();
