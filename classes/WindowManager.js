const {app, BrowserWindow, shell, ipcMain} = require("electron");
const fs = require("fs");
const path = require("path");
const i18nextBackend = require("i18next-electron-fs-backend");

const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const i18nextMainBackend = Config.require("i18n/i18n.backend");
const MenuManager = Config.require("classes/MenuManager");

class WindowManager {
    #currentLanguage;
    #languagesLoaded;
    #globalCssPaths;
    #globalJsPaths;
    #translations;
    #windows;

    constructor() {
        this.#currentLanguage = app.getLocale();
        this.#languagesLoaded = false;
        this.#globalCssPaths = [
            Config.projectPath("node_modules/bootstrap/dist/css/bootstrap.min.css"),
        ];
        this.#globalJsPaths = [
            Config.projectPath("node_modules/@popperjs/core/dist/umd/popper.min.js"),
            Config.projectPath("node_modules/bootstrap/dist/js/bootstrap.min.js"),
            Config.projectPath("node_modules/jquery/dist/jquery.min.js"),
            Config.projectPath("node_modules/i18next/i18next.min.js"),
            Config.projectPath("node_modules/jquery-i18next/jquery-i18next.min.js"),
            Config.projectPath("node_modules/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.min.js"),
            Config.modulePath("i18n/i18n.frontend.js"),
            Config.modulePath("assets/controllers/app.js"),
        ];
        this.#windows = {};

        let languages;
        i18nextMainBackend.on("initialized", (options) => {
            i18nextMainBackend.on("failedLoading", (language, namespace, message) => {
                console.error("fail", language, namespace, message);
            });

            i18nextMainBackend.on("languageChanged", language => {
                this.#currentLanguage = language;
                this.changeLanguage(language);
            });

            languages = options.supportedLngs.filter(language => {
                return language.length === 2;
            });

            this.#translations = {};
            i18nextMainBackend
                .loadLanguages(languages)
                .then(() => {
                    languages.forEach(language => {
                        this.#translations[language] = i18nextMainBackend.getDataByLanguage(language);
                    });
                    this.#languagesLoaded = true;
                    i18nextMainBackend.changeLanguage(app.getLocale());
                })
                .catch(err => {
                    console.error(err);
                });
        });
    }

    get currentLanguage() {
        return this.#currentLanguage;
    }

    get globalCssPaths() {
        return this.#globalCssPaths;
    }

    get globalJsPaths() {
        return this.#globalJsPaths;
    }

    get translations() {
        return typeof this.#translations !== "undefined" ? this.#translations : null;
    }

    addGlobalCssPath(cssPath) {
        this.#globalCssPaths.push(cssPath);

        Object.keys(this.#windows).forEach(windowName => {
            Object.keys(this.#windows[windowName]).forEach(windowId => {
                const window = this.#windows[windowName][windowId];
                window.useStyle([cssPath]);
            });
        });
    }

    addGlobalJsPath(jsPath) {
        this.#globalJsPaths.push(jsPath);

        Object.keys(this.#windows).forEach(windowName => {
            Object.keys(this.#windows[windowName]).forEach(windowId => {
                const window = this.#windows[windowName][windowId];
                window.executeScripts([jsPath]);
            });
        });
    }

    changeLanguage(language) {
        if (i18nextMainBackend.isInitialized) {
            this.#currentLanguage = language;

            console.log("New language :", language);

            if (process.platform === "darwin") {
                MenuManager.initMenu(i18nextMainBackend, BrowserWindow.getFocusedWindow());
            }

            Object.keys(this.#windows).forEach(windowName => {
                Object.keys(this.#windows[windowName]).forEach(windowId => {
                    const window = this.#windows[windowName][windowId];
                    if (process.platform !== "darwin") {
                        MenuManager.initMenu(i18nextMainBackend, window);
                    }
                    window.webContents.send("onChangeLanguage", this.#currentLanguage);
                });
            });
        }
    }

    create(window) {
        if (typeof this.#windows[window.windowName] !== "undefined" && typeof this.#windows[window.windowName][window.windowId] !== "undefined") {
            if (this.#windows[window.windowName][window.windowId].shown) {
                this.#windows[window.windowName][window.windowId].close();
            } else {
                return;
            }
        }

        if (typeof this.#windows[window.windowName] === "undefined") {
            this.#windows[window.windowName] = {};
        }

        this.#windows[window.windowName][window.windowId] = window;

        if (Config.mainWindow === null) {
            let win = window;
            do {
                Config.mainWindow = win;
            } while (win.getParentWindow() !== null);
        }

        i18nextBackend.mainBindings(ipcMain, window, fs);

        if(this.#languagesLoaded) {
            MenuManager.initMenu(i18nextMainBackend, window);
        }

        window.addWindowListeners({
            "focus": () => {
                if (process.platform === "darwin") {
                    MenuManager.initMenu(i18nextMainBackend, window);
                }
            },
            "close": () => {
                if (Config.mainWindow !== null && Config.mainWindow.windowId === window.windowId && Config.mainWindow.windowName === window.windowName) {
                    Config.mainWindow = null;
                }

                if (typeof this.#windows[window.windowName] !== "undefined" && typeof this.#windows[window.windowName][window.windowId] !== "undefined") {
                    delete this.#windows[window.windowName][window.windowId];
                    if (Object.keys(this.#windows[window.windowName]).length === 0) {
                        delete this.#windows[window.windowName];
                    }
                }
            }
        }, true);

        if (!window.shown) {
            window.addWindowListeners({
                "ready-to-show": () => {
                    window.show();
                }
            });
        }

        window.prepareWindow();

        return window;
    }

    externalSiteRequested = (_, url) => {
        shell.openExternal(url).then().catch(err => {
            console.error(err);
        });
    };
}

module.exports = new WindowManager();
