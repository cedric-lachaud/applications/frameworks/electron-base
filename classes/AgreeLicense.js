const fs = require("fs");
const path = require("path");
const Config = require(path.join(globalThis.moduleDir, "classes/Config"));
const {app} = require("electron");
const WindowManager = Config.require("classes/WindowManager");
const WindowLicense = require(Config.licenseWindowClassPath);

class AgreeLicense {
    static check(startCallback) {
        const mainDirectory = fs.readdirSync(Config.projectDir);

        if (mainDirectory.includes(`LICENSE.txt`)) {
            if (!mainDirectory.includes("eula.txt")) {
                const licenseText = fs.readFileSync(path.join(Config.projectDir, "LICENSE.txt")).toString();

                const window = WindowManager.create(new WindowLicense(licenseText));
                Config.mainWindow = null;

                window.addWindowListeners({
                    "agree-license": () => {
                        startCallback();
                        window.close();
                    },
                    "disagree-license": () => {
                        app.quit();
                    }
                });
            } else {
                fs.readFile(path.join(Config.projectDir, "eula.txt"), {}, (err, data) => {
                    if (err) {
                        console.log(err);
                    } else {
                        if (data.toString() === "agree") {
                            startCallback();
                        } else {
                            app.quit();
                        }
                    }
                });
            }
        } else {
            startCallback();
        }
    }
}

module.exports = AgreeLicense;